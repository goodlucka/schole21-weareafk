<?php
class ArticleManager {
    // return all articles in pack
    public function returnArticles($url) {
        return DB::queryAll('
            SELECT a.id, a.url, a.title, a.content
            FROM articles a, articles_packs ap, packs p
            WHERE a.id=ap.ida 
            AND ap.idp=p.id 
            AND p.url = ?
            ORDER BY a.title
        ', array($url));
    }
    // return only one article
    public function returnArticle($url) {
        return DB::queryRow('
            SELECT a.id, a.url, a.title, a.content 
            FROM articles a 
            WHERE a.url = ?
        ',array($url));
    }
    // return all articles
    public function returnAllArticles() {
        return DB::queryAll('
            SELECT a.id, a.url, a.title, a.content 
            FROM articles a
            ORDER BY title
        ');
    }
    // add new article
    public function newArticle($url,$title,$content) {
        if($url=="new") throw new Exception();
        return DB::queryRow('
            INSERT INTO articles(url,title,content)
            VALUES (?,?,?)
        ',array($url,$title,$content));
    }
    // update article content
    public function updateArticle($url,$content) {
        return DB::queryRow('
            UPDATE articles
            SET content = ?
            WHERE url = ?
        ',array($content,$url));
    }
    // delete article
    public function deleteArticle($url) {
        return DB::queryAll('
            DELETE FROM articles
            WHERE url = ?
        ', array($url));
    }
     // return all articles, if it is in pack url, then 1 else 0
    public function returnArticlesInPack($url) {
        return DB::queryAll('
            SELECT a.url, a.title, a.content,
                CASE
                    WHEN a.id IN (
                        SELECT ap.ida
                        FROM articles_packs ap, packs p
                        WHERE ap.idp = p.id
                        AND p.url = ?
                        )
                        THEN 1
                    ELSE 0
                END AS in_pack
            FROM articles a
            ORDER BY a.title
        ',array($url));
    }
    // convert urls->ids of articles
    public function returArticlesIds($articles) {
        return DB::queryAll("
            SELECT id
            FROM articles
            WHERE url IN ('".implode("','",$articles)."')
        ");
    }
}