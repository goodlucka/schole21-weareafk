<?php
class PageManager {
    // return this page
    public function returnPage($page) {
        return DB::queryRow('
            SELECT page, pagename, content 
            FROM pages
            WHERE page = ?
        ', array($page));
    }
    // return all pages
    public function returnAllPages() {
        return DB::queryAll('
            SELECT page, pagename, content
            FROM pages
        ');
    }
    // update page content
    public function updatePage($page,$content) {
        return DB::queryRow('
            UPDATE pages
            SET content = ?
            WHERE page = ?
        ',array($content,$page));
    } 
}