<?php
class PackManager {
    // return all packs 
    public function returnAllPacks() {
        return (DB::queryAll('
            SELECT p.id, p.url, p.title, p.introduction, p.createdate
            FROM packs p
        '));
    }
    // return all packs order by date (newest first)
    public function returnAllPacksOrderByDate() {
        return (DB::queryAll('
            SELECT p.id, p.url, p.title, p.introduction, p.createdate,
                CASE
                    WHEN p.id IN (
                        SELECT id
                        FROM users_packs
                        WHERE userlogin = ?)
                        THEN 1
                    ELSE 0
                END AS bought
            FROM packs p
            ORDER BY p.createdate DESC
        ',array($_SESSION['user']['userlogin'])));
    }
    // return all packs order by name (a-z)
    public function returnAllPacksOrderByName() {
        return (DB::queryAll('
            SELECT p.id, p.url, p.title, p.introduction, p.createdate,
                CASE
                    WHEN p.id IN (
                        SELECT id
                        FROM users_packs
                        WHERE userlogin = ?)
                        THEN 1
                    ELSE 0
                END AS bought
            FROM packs p
            ORDER BY p.title ASC
        ',array($_SESSION['user']['userlogin'])));
    }    
    // return url if pack is bought
    public function isBought($url) {
        return DB::queryRow('
            SELECT p.url as purl
            FROM packs p, users_packs up 
            WHERE up.id = p.id
            AND up.userlogin = ?
            AND p.url = ?
        ', array($_SESSION['user']['userlogin'],$url));
    }
    //return any pack matters on url
    public function returnPack($url) {
        return DB::queryRow('
            SELECT p.id, p.url, p.title, p.introduction, p.createdate 
            FROM packs p
            WHERE p.url = ?
        ',array($url));
    }
    // return all videos and all articles which are part of bought pack
    public function returnBoughtPack($url) {
        $videoManager = new VideoManager();
        $articleManager = new ArticleManager();
        return array(
            "videos" => $videoManager->returnVideos($url),
            "articles" =>$articleManager->returnArticles($url)
        );
    }
    // buy pack
    public function buyPack($url) {
        /*
         *              !!!!!!!!
         *              PŘIDAT
         *              PLATEBNÍ
         *              BRÁNU
         *              !!!!!!!!
         */
        return DB::queryRow('
            INSERT INTO users_packs(id, userlogin)
            VALUES ((
                    SELECT p.id 
                    FROM packs p 
                    WHERE p.url = ?)
                , ?)
        ',array($url,$_SESSION['user']['userlogin']));
    }
    // add new pack into DB
    public function newPack($url,$title,$introduction) {        
        if($url=="new") throw new Exception();
        return DB::queryRow('
            INSERT INTO packs(url,title,introduction)
            VALUES (?,?,?)
        ',array($url,$title,$introduction));
    }
    // delete pack from DB
    public function deletePack($url) {
        return DB::queryAll('
            DELETE FROM packs
            WHERE url = ?
        ', array($url));
    }
    // update introduction
    public function updatePack($url,$introduction) {
        return DB::queryRow('
            UPDATE packs
            SET introduction = ?
            WHERE url = ?
        ',array($introduction,$url)); 
    }
    // update pack (update videos/articles)
    public function updatePackData($data,$packurl) {
        $videos = array();
        $articles = array();
        foreach ($data as $d) {
            if(str_starts_with($d, "v_")) // if video
                array_push($videos,substr($d,2));
            else if(str_starts_with($d, "a_")) // if article
                array_push($articles,substr($d,2));
        }
        $pack=$this->returnPack($packurl);
        $pack = $pack["id"];

        // delete everything
        DB::queryAll('
            DELETE FROM videos_packs
            WHERE idp = ?
        ',array($pack));
        DB::queryAll('
            DELETE FROM articles_packs
            WHERE idp = ?
        ',array($pack));
        
        // add everything
        if(!empty($videos))
            $this->insertVideos($videos,$pack);
        if(!empty($articles))
            $this->insertArticles($articles,$pack);
    }
    // insert data to videos_packs
    public function insertVideos($videos,$pack) {
        $videoManager = new VideoManager();
        $videosid = $videoManager->returnVideosIds($videos);
        $vids = array();
        foreach($videosid as $key => $id) {
            array_push($vids,$id["id"]);
        }
        $query = "INSERT INTO videos_packs (idp,idv) VALUES ";
        $query_parts = array();
        for($i=0; $i<count($vids); $i++){
            $query_parts[] = "(" . $pack . ", " . $vids[$i] . ")";
        }
        $query .= implode(',', $query_parts);
        DB::queryAll($query);
    }
    // insert data to articles_packs
    public function insertArticles($articles,$pack) {
        $articleManager = new ArticleManager();
        $articlesid = $articleManager->returArticlesIds($articles);
        $aids = array();
        foreach($articlesid as $key => $id) {
            array_push($aids,$id["id"]);
        }
        $query = "INSERT INTO articles_packs (idp,ida) VALUES ";
        $query_parts = array();
        for($i=0; $i<count($aids); $i++){
            $query_parts[] = "(" . $pack . ", " . $aids[$i] . ")";
        }
        $query .= implode(',', $query_parts);
        DB::queryAll($query);
    }
}