<?php
class VideoManager {
    // return all videos in pack
    public function returnVideos($url) {
        return DB::queryAll('
            SELECT v.id, v.url, v.title, v.content
            FROM videos v, videos_packs vp, packs p
            WHERE v.id=vp.idv 
            AND vp.idp=p.id 
            AND p.url = ?
            ORDER BY v.title
        ', array($url));
    }
    // return only one video
    public function returnVideo($url) {
        return DB::queryRow('
            SELECT v.id, v.url, v.title, v.content 
            FROM videos v 
            WHERE v.url = ?
        ',array($url));
    }
    // return all videos
    public function returnAllVideos() {
        return DB::queryAll('
            SELECT v.id,v.url,v.title,v.content
            FROM videos v
            ORDER BY v.title
        ');
    }
    // add new video
    public function newVideo($url,$title,$content) {
        if($url=="new") throw new Exception("Chyba při ukládání videa, název videa je neplatný.");
        if(!isset($_FILES["video"])) throw new Exception("Nebyl vybrán žádný soubor.");
        $video = $_FILES["video"];
        if(!$video['type']=='video/mp4') throw new Exception("Podporovaný formát videa je pouze .mp4");
        
        $temp_name = $video['tmp_name'];

        if(!move_uploaded_file($temp_name,"videos/".$url.".mp4"))
            throw new Exception('Chyba při ukládání videa.');    
        
        return DB::queryRow('
            INSERT INTO videos (url, title, content)
            VALUES (?,?,?)
        ',array($url,$title,$content));
    }
    // update video content
    public function updateVideo($url,$content) {
        return DB::queryRow('
            UPDATE videos
            SET content = ?
            WHERE url = ?
        ',array($content,$url));
    }
    // delete video
    public function deleteVideo($url) {
        if (file_exists("videos/".$url.".mp4"))
            unlink("videos/".$url.".mp4");
        return DB::queryAll('
            DELETE FROM videos
            WHERE url = ?
        ', array($url));
    }
    // return all videos, if it is in pack url, then 1 else 0
    public function returnVideosInPack($url) {
        return DB::queryAll('
            SELECT v.url, v.title, v.content,
                CASE
                    WHEN v.id IN (
                        SELECT vp.idv
                        FROM videos_packs vp, packs p
                        WHERE vp.idp = p.id
                        AND p.url = ?
                        )
                        THEN 1
                    ELSE 0
                END AS in_pack
            FROM videos v
            ORDER BY v.title
        ',array($url));
    }
    // convert urls->ids of videos
    public function returnVideosIds($videos) {
        return DB::queryAll("
            SELECT id
            FROM videos
            WHERE url IN ('".implode("','",$videos)."')
        ");
    }
    // return bought video, if video exists and it is bought by user
    public function returnBoughtVideo($urlp,$urlv) {
        return DB::queryRow('
            SELECT v.url 
            FROM videos v, videos_packs vp, packs p, users_packs up
            WHERE   v.id = vp.idv AND
                    vp.idp = p.id AND
                    p.id = up.id AND
                    p.url = ?  AND
                    v.url = ? AND
                    up.userlogin = ?
        ',array($urlp,$urlv,$_SESSION["user"]["userlogin"]));
    }
}