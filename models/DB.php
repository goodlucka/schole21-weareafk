<?php
class DB {
    private static $connection;
    private static $settings = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false,
    );
    // connect to DB
    public static function connect($server, $user, $password, $db) {
        if (!isset(self::$connection))
        {
            self::$connection = @new PDO(
                "mysql:host=$server;dbname=$db",
                $user,
                $password,
                self::$settings
            );
        }   
    }
    // sql query for one row
    public static function queryRow($query, $param = array()) {
        $result = self::$connection->prepare($query);
        $result->execute($param);
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    // sql query for all rows
    public static function queryAll($query, $param = array()) {
        $result = self::$connection->prepare($query);
        $result->execute($param);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    // sql query for 1 col 1 row
    public static function queryCol($query, $param = array()) {
        $result = self::queryRow($query, $param);
        return $result[0];
    }
    // sql query for more cols more rows
    public static function query($query, $param = array()) {
        $result = self::$connection->prepare($query);
        $result->execute($param);
        return $result->rowCount();
    }
    // insert into db
    public static function insert($table, $param = array()) {
        return self::query("INSERT INTO `$table` (`".
            implode('`, `', array_keys($param)).
            "`) VALUES (".str_repeat('?,', sizeOf($param)-1)."?)",
                array_values($param));
    }
}