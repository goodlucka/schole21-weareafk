<?php
class EmailSender {
    // send email
    public function send($to, $subject, $message, $from) {
        $head = "MIME-Version: 1.0" . "\r\n";
        $head .= "Content-Type:text/html;charset=\"utf-8\"" . "\r\n";
        $head .= "From: " . $from . "\r\n";
        $a=mail($to, $subject, $message, $head);
        if (!$a)
            throw new ErrorUser('Email se nepodařilo odeslat.');
    }
    // send email with antispam
    public function sendWithAntispam($year, $to, $subject, $message, $from) {
        if ($year != date("Y"))
            throw new ErrorUser('Chybně vyplněný antispam.');
        $this->send($to, $subject, $message, $from);
    }
}