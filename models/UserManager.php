<?php
class UserManager {
    // return hash
    public function hashPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    // return right answer for antispam
    public static function antispam($data) {
        return ($data==date('Y')); // this year
    }
    // valid password, return $e
    public static function checkPassword($password, $password2) {
        $e = "";
        if($password != $password2) {
            $e.='Hesla nejsou stejná. ';
        } else {
            if (preg_match("/\\s/", $password))
                $e.='Heslo obsahuje nepovolené znaky. ';
            if(strlen($password)<8)
                $e.='Heslo musí mít minimálně 8 znaků. ';
                else if(strlen($password)>40)
                    $e.='Heslo musí mít maximálně 40 znaků. ';
            if(!preg_match("@[0-9]@",$password))
                $e.='Heslo musí obsahovat minimálně jedno číslo. ';
            if(!preg_match("@[a-zA-Z]@",$password))
                $e.='Heslo musí obsahovat minimálně jedno písmeno. ';
        }
        return $e;
    }
    // valid userlogin, return $e
    public static function checkLogin($login) {
        $e = "";
        if (!preg_match("/^[a-z0-9]+$/",$login))
            $e.='Přihlašovací jméno musí obsahovat pouze malá písmena nebo čísla. ';
        if(strlen($login)<6)
            $e.='Přihlašovací jméno musí mít minmálně 6 znaků. ';
            else if (strlen($login)>20)
                $e.='Přihlašovací jméno musí mít maximálně 20 znaků. ';
        return $e;
    }
    // valid name, return $e
    public static function checkName($name) {
        $e = "";
        if(!preg_match("/^[a-zA-ZáčďéěíňóřšťůúýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽäëïöüÿ \\\']+$/", $name))
            $e.='Neplatné jméno. ';
        return $e;
    }
    // add new user to DB (register)
    public function register($login, $password, $password2, $username, $email, $antispam, $code) {
        if (!self::antispam($antispam))
            throw new ErrorUser('Chybně vyplněný antispam.');
        $e = self::checkPassword($password, $password2);
        $e .= self::checkLogin($login);
        $e .= self::checkName($username);
        if ($e!="")
            throw new ErrorUser($e); // if there is something wrong, throw exception
        $user = array(
            'userlogin' => $login,
            'username' => $username,
            'passwd' => $this->hashPassword($password),
            'email' => $email,
            'role' => 1,
            'code' => $code
        );
        // register user
        try {
            Db::insert('users', $user);
        } catch (PDOException $e) {
            throw new ErrorUser('Uživatel s tímto jménem je již zaregistrovaný.');
        }
    }
    // confirm register by mail
    public function confirmRegister($code, $u) {
        $login = DB::queryRow('
            SELECT userlogin
            FROM users
            WHERE code = ?
            ',array($code));
        if($login == null) 
            throw new ErrorUser();
        if(sha1($code.$login['userlogin'])!=$u)
            throw new ErrorUser();
        DB::queryRow('
            UPDATE users 
            SET role = 2,
                code = null
            WHERE userlogin = ?
        ', array($login['userlogin']));
    }
    // login user, set session
    public function login($login, $password) {
        $user = $this->returnUserData($login);
        $picture = "images/0.jpg";
        if(!$user || !password_verify($password, $user['passwd']))
            throw new ErrorUser('Neplatné jméno nebo heslo.');
        if($user['role'] <= 1)
            throw new ErrorUser('Neaktivní účet.');
        $path  = $user['picture'];
        if(file_exists($path)) {
            $picture = $user['picture'];
        }$_SESSION['user'] = array(
            "userlogin" => $user["userlogin"],
            "role" => $user["role"],
            "username" => $user["username"],
            "email" => $user["email"],
            "role" => $user["role"],
            "picture" => $picture
        );
    }
    // update user profile
    public function update($post) {

        // check inserted data
        $password = "";
        if(isset($post["password"])) $password = $post["password"];
        $password1 = "";
        if(isset($post["password1"])) $password1 = $post["password1"];
        $password2 = "";
        if(isset($post["password2"])) $password2 = $post["password2"];
        $email = "";
        if(isset($post["email"])) $email = $post["email"];
        $newpicture = "";
        if(isset($_FILES["newpicture"])) $newpicture = $_FILES["newpicture"];
  
        // check old password
        if(!$this->comparePasswords($password))
            throw new ErrorUser("Zadali jste špatné staré heslo.");
        else { // right password
            if($newpicture["size"]>0){ // change picture
                $e = "";
                if($newpicture['size']>40960)   // max size
                    $e.='Obrázek je moc velký. Maximální velikost obrázku je 40KB. ';
                if(!($newpicture['type']=='image/png' ||    // picture type
                    $newpicture['type']=='image/jpg' || 
                    $newpicture['type']=='image/jpeg' ||
                    $newpicture['type']=='image/gif'))
                    $e.='Nepodporovaný typ obrázku. ';
                if($e != "")
                    throw new ErrorUser($e);
                
                // picture data
                $target_dir = "images/";
	            $file = $newpicture['name'];
	            $path = pathinfo($file);
	            $filename = sha1($_SESSION['user']['userlogin'].time()); // random name
	            $ext = $path['extension'];
	            $temp_name = $newpicture['tmp_name'];
	            $path_filename_ext = $target_dir.$filename.".".$ext;
    
                // save picture to server
                if(!move_uploaded_file($temp_name,$path_filename_ext))
                    throw new ErrorUser('Chyba při ukládání obrázku.');    
                
                // update DB with new picture link
                DB::queryRow('
                    UPDATE users
                    SET picture = ?
                    WHERE userlogin = ?
                ', array($path_filename_ext, $_SESSION['user']['userlogin']));

                // chceck name of old picture
                $oldpicture = $_SESSION['user']['picture'];
                if (file_exists("images/".$oldpicture) && $oldpicture != "0.jpg")
                    unlink("images/".$oldpicture); // delete old picture from server

                $_SESSION['user']['picture'] = "images/".$filename.".".$ext; // set new picture to session
            } // change picture

            // change password
            if($password1!="") {
                if($password1 != $password2)
                    throw new ErrorUser("Hesla musejí být stejná.");
                else {
                    $e = $this->checkPassword($password1, $password2);
                    if ($e!="")
                        throw new ErrorUser($e);
                    DB::queryRow('
                        UPDATE users 
                        SET passwd = ?
                        WHERE userlogin = ?
                    ', array($this->hashPassword($password1), $_SESSION['user']['userlogin']));
                }
            } // change password

            // change email
            if($email!="") {
                echo "ne-měním email, možná jindy sorry";     // kontrola mailuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu
            } // change email

        } // right password
    }
    // logout user
    public function logout() {
        unset($_SESSION['user']);  
    }
    // return logged user
    public function returnUser() {
        if (isset($_SESSION['user']))
            return $_SESSION['user'];
        return null;
    }
    // return user["userlogin","username",...]
    private function returnUserData($login) {
        return DB::queryRow('
            SELECT userlogin, username, passwd, email, role, picture
            FROM users
            WHERE userlogin = ?
        ', array($login));
    }
    // compare passwords of logged user and $password
    public function comparePasswords($password) {
        $loggedUser = $this->returnUser();
        $userData = null;
        if($loggedUser)
            $userData = $this->returnUserData($loggedUser['userlogin']);
        return password_verify($password, $userData['passwd']);
    }

}