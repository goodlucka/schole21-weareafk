use schole21;

insert into articles (url, title, content)
values ('clanek-cislo-1', 'Článek číslo 1 :)', '<p>Nějaký text článku</p>');
insert into articles (url, title, content)
values ('clanek-cislo-2', 'Článek číslo 2 :)', '<p>Nějaký text článku</p>');
insert into articles (url, title, content)
values ('clanek-cislo-3', 'Článek číslo 3 :)', '<p>Nějaký text článku</p>');
insert into articles (url, title, content)
values ('clanek-cislo-4', 'Článek číslo 4 :)', '<p>Nějaký text článku</p>');
insert into articles (url, title, content)
values ('clanek-cislo-5', 'Článek číslo 5 :)', '<p>Nějaký text článku</p>');

insert into videos (url, title, content)
values ('video-prvni', 'Video - první', '<p>Nějaký popis videa</p>');
insert into videos (url, title, content)
values ('video-druhe', 'Video - druhé', '<p>Nějaký popis videa</p>');

insert into packs (url, title, introduction)
values ('balicek-jedna', 'Balíček jedna', '<p>Nějaký popis balíčku</p>');
insert into packs (url, title, introduction)
values ('balicek-dva', 'Balíček dva', '<p>Nějaký popis balíčku</p>');
insert into packs (url, title, introduction)
values ('balicek-tri', 'Balíček tři', '<p>Nějaký popis balíčku</p>');


insert into articles_packs (idp, ida) 
values (1,1);
insert into articles_packs (idp, ida) 
values (2,2);
insert into articles_packs (idp, ida) 
values (1,3);
insert into articles_packs (idp, ida) 
values (2,4);
insert into articles_packs (idp, ida) 
values (1,5);

insert into videos_packs (idp, idv) 
values (1,1);
insert into videos_packs (idp, idv) 
values (2,2);
insert into videos_packs (idp, idv) 
values (2,1);