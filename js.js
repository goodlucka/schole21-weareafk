$(document).ready(function() {
    // sort packs
    $('input[type=checkbox][name=view_bought]').change(function() {
        $(".pack-bought").toggle("slow");
    });
    $('input[type=checkbox][name=view_avaliable]').change(function() {
        $(".pack-avaliable").toggle("slow");
    });
    $('input[type=radio][name=sort]').change(function() {
        if(this.value == 'sort_date') {
            $(".packs-name").fadeToggle(function() {
                $(".packs-date").fadeToggle(800);
            });
        }
        if(this.value == 'sort_name') {
            $(".packs-date").fadeToggle(function() {
                $(".packs-name").fadeToggle(800);
            });
        }
    });
    // sort packs

    tinymce.init({
        selector: '.tinyarea',
        language: 'cs',
        toolbar_mode: 'sliding',
        entities: '160,nbsp',
        entity_encoding: 'named',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    }); // tinymce
});

function navResponsive() {
    // toggle menu
    var x = document.getElementById("navbar");
    if (x.className === "navbar") {
        x.className += " responsive";
    } else {
        x.className = "navbar";
    }
    var x = document.getElementById("right-col");
    if (x.className === "right-col") {
        x.className += " responsive";
    } else {
        x.className = "right-col";
    }
    // toggle menu
}   

    // adminpage - delete article
function deleteArticle(url) {
    if(confirm("Vážně chcete smazat tento článek? Akce je nevratná!")) {
        window.location.replace("administrace/clanky/"+url+"/delete");
    }
}
    // adminpage - delete article

    // adminpage - delete video
function deleteVideo(url) {
    if(confirm("Vážně chcete smazat toto video? Akce je nevratná!")) {
        window.location.replace("administrace/videa/"+url+"/delete");
    }
}
    // adminpage - delete article


    // adminpage - delete pack
function deletePack(url) {
    if(confirm("Vážně chcete smazat tento balíček? Akce je nevratná!")) {
        window.location.replace("administrace/balicky/"+url+"/delete");
    }
}
    // adminpage - delete pack

