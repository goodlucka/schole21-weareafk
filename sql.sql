CREATE DATABASE IF NOT EXISTS schole21 DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
    userlogin VARCHAR(20) COLLATE utf8_czech_ci NOT NULL COMMENT 'login',
    username VARCHAR(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'fullname or nickname',
    passwd VARCHAR(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'passwd',
    email VARCHAR(40) COLLATE utf8_czech_ci NOT NULL COMMENT 'reg e-mail',
    role INT(1) NOT NULL COMMENT '9 = admin, 2 = normal user, 1 = unauthorized, 0 = banned',
    picture VARCHAR(70) COLLATE utf8_czech_ci NOT NULL DEFAULT '0.jpg' COMMENT 'profile picture',
    code VARCHAR(40) COLLATE utf8_czech_ci COMMENT 'sha1 hash auth code',
    PRIMARY KEY (userlogin)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS packs;
CREATE TABLE IF NOT EXISTS packs (
    id INT(7) NOT NULL AUTO_INCREMENT,
    url VARCHAR(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'unique url',
    title VARCHAR(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'title of pack',
    introduction TEXT COLLATE utf8_czech_ci COMMENT 'pack intro',
    createdate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'time of create',
    PRIMARY KEY (id),
    CONSTRAINT packs_url UNIQUE(url)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS users_packs;
CREATE TABLE IF NOT EXISTS users_packs (
    id INT(7) NOT NULL COMMENT 'pack id',
    userlogin VARCHAR(20) COLLATE utf8_czech_ci NOT NULL COMMENT 'user login',
    paytime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'time of payment',
    PRIMARY KEY(id, userlogin),
    CONSTRAINT users_packs_pfk FOREIGN KEY(id) REFERENCES packs(id),
    CONSTRAINT users_packs_ufk FOREIGN KEY(userlogin) REFERENCES users(userlogin)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS articles;
CREATE TABLE IF NOT EXISTS articles (
    id INT(7) NOT NULL AUTO_INCREMENT,
    url VARCHAR(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'unique url',
    title VARCHAR(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'title of article',
    content TEXT COLLATE utf8_czech_ci COMMENT 'content of article',
    PRIMARY KEY(id),
    CONSTRAINT articles_url UNIQUE(url)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS articles_packs;
CREATE TABLE IF NOT EXISTS articles_packs (
    idp INT(7) NOT NULL COMMENT 'pack id',
    ida INT(7) NOT NULL COMMENT 'article id',
    PRIMARY KEY(idp, ida),
    CONSTRAINT articles_packs_pfk FOREIGN KEY(idp) REFERENCES packs(id) ON DELETE CASCADE,
    CONSTRAINT articles_packs_afk FOREIGN KEY(ida) REFERENCES articles(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS videos;
CREATE TABLE IF NOT EXISTS videos (
    id INT(7) NOT NULL AUTO_INCREMENT,
    url VARCHAR(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'unique url',
    title VARCHAR(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'title of video',
    content TEXT COLLATE utf8_czech_ci NOT NULL COMMENT 'content of video',
    PRIMARY KEY(id),
    CONSTRAINT videos_url UNIQUE(url)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS videos_packs;
CREATE TABLE IF NOT EXISTS videos_packs (
    idp INT(7) NOT NULL COMMENT 'pack id',
    idv INT(7) NOT NULL COMMENT 'video id',
    PRIMARY KEY(idp, idv),
    CONSTRAINT videos_packs_pfk FOREIGN KEY(idp) REFERENCES packs(id) ON DELETE CASCADE,
    CONSTRAINT videos_packs_vfk FOREIGN KEY(idv) REFERENCES videos(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS pages;
CREATE TABLE IF NOT EXISTS pages (
    pagelink VARCHAR(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'page link',
    pagename VARCHAR(50) COLLATE utf8_czech_ci NOT NULL COMMENT 'page name',
    content TEXT COLLATE utf8_czech_ci COMMENT 'page content',
    PRIMARY KEY (page)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;