<?php
class PodminkyController extends Controller {
    public function run($param) {
        $this->head = array(   
            "title" => "Podmínky pro používání scholé21",
            "keywords" => "",
            "description" => "Podmínky pro používání - schole21.cz"
        );

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("privacy-policy");
        $this->view = "_base";
    }
}