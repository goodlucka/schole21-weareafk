<?php
class ProfilController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Profil",
            "keywords" => "",
            "description" => "Profil scholé21.cz"
        );
        if($_SESSION) { // if logged user
            try { 
                if(!empty($_POST)) { // if $_POST is set try to update data
                    $userManager = new UserManager();
                    $userManager->update($_POST);
                    $this->addMessage(array(
                        'type' => 'success',
                        'data' => 'Údaje úspěšně změněny'));
                    }
            } catch (ErrorUser $e) {
                $this->addMessage(array(
                    'type' => 'error',
                    'data' => $e->getMessage()));  
            } catch (PDOException $e) {
                $this->addMessage(array(
                    'type' => 'error',
                    'data' => 'Chyba při ukládání do DB.'));
            }
            $this->view = "profile";    // set profile view
        } else { // if nologged user, redirect to login
            $this->addMessage(array(
                'type' => 'warning',
                'data' => 'Nepřihlášený uživatel'
            ));
            $this->redirect("prihlaseni");
        }
    }
}