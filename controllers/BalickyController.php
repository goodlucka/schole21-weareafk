<?php
class BalickyController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Balíčky",
            "keywords" => "",
            "description" => "Balíčky scholé21.cz"
        );
        if($this->verifyUser(2)) { // logged user
            if(!empty($param[0])) { // balicky/packurl
                $packManager = new PackManager();
                if(isset($param[1])) 
                    if($param[1]=="buy") { // balicky/packurl/buy
                        try {   // try to buy pack, add something like redirect from gopay
                            $packManager->buyPack($param[0]);
                            $this->addMessage(array(
                                "type" => "success",
                                "data" => "Balíček úspěšně zakoupený. (mezi tím kliknutím bude vstup na platební bránu, zaplacení a návrat na tuhle stránku."
                            ));
                        } catch (PDOException) {
                            $this->addMessage(array(
                                "type" => "warning",
                                "data" => "Balíček už je zakoupený."
                            ));
                        }
                        $this->redirect("balicky/".$param[0]);
                }

                if($packManager->isBought($param[0])) { // balicky/packurl if it is bought
                    $this->data["pack"] = $packManager->returnPack($param[0]); // $pack - info

                    if(isset($param[1])) { // balicky/packurl/spomething
                        if($param[1]=="video") { // balicky/packurl/video/
                            if(isset($param[2])) { // balicky/packurl/video/videourl
                                $videoManager = new VideoManager();
                                $video = $videoManager->returnVideo($param[2]);
                                if (!$video)
                                    $this->redirect('error');   // if video doesnt exists, redirect to error page
                                // else print video page
                                $this->data['video'] = $video;
                                $this->view = '_video';
                            } else $this->redirect("balicky/".$param[0]);
                        } else if($param[1]=="clanek") { // balicky/packurl/clanek/articleurl
                            if(isset($param[2])) {
                                $articleManager = new ArticleManager();
                                $article = $articleManager->returnArticle($param[2]);
                                if (!$article)
                                    $this->redirect('error');  // if article doesnt exists, redirect to error page
                                // else print article page
                                $this->data['article'] = $article;
                                $this->view = '_article';
                            } else $this->redirect("balicky/".$param[0]);
                        }
                    } else { // balicky/packurl - pack info, list of videos, articles
                        $this->data["packdata"] = $packManager->returnBoughtPack($param[0]);
                        if(empty($this->data["packdata"]["videos"]))
                            $this->addMessage(array(
                                "type" => "warning",
                                "data" => "Žádné videa v balíčku."
                            ));
                        if(empty($this->data["packdata"]["articles"]))
                            $this->addMessage(array(
                                "type" => "warning",
                                "data" => "Žádné články v balíčku."
                            ));
                        $this->view = "_pack_bought";
                    }
                } else { // not bought pack, show balicky/packurl info
                    $this->data["pack"] = $packManager->returnPack($param[0]);
                    $this->view = "_pack";
                }

            } else { // print all packs balicky/
                $packManager = new PackManager();
                $this->data["allpacksdate"] = $packManager->returnAllPacksOrderByDate();
                $this->data["allpacksname"] = $packManager->returnAllPacksOrderByName();
                $this->view = "packs";
            }
        } else { // not authorized user (ban, not confirmed mail, not logged user) -> redirect to home page
            $this->addMessage(array(
                'type' => 'warning',
                'data' => 'Nedostatečná oprávnění.'
            ));
            $this->redirect("home");
        }
    }
}