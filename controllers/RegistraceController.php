<?php
class RegistraceController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Registrace",
            "keywords" => "",
            "description" => "Registrační formulář schole21.cz"
        );

        if(isset($_SESSION['user'])) // if logged user want to register again, redirect to home page
            $this->redirect('home');
        else { // if user is not logged
            $this->view = "register"; // set this view default to register
            if(isset($_GET['code'])&&isset($_GET['u'])) {   // if user want to confirm mail
                try { // try to confirm email, update user to confirmed user
                    $userManager = new  UserManager();
                    $userManager->confirmRegister($_GET['code'],$_GET['u']);
                    $this->addMessage(array(
                        'type' => 'success',
                        'data' => 'Registrace úspěšná, nyní se můžete přihlásit.'));
                    $this->view = "login"; // everything is ok - view login page
                } catch (ErrorUser $e) {
                    $this->addMessage(array(
                        'type' => 'error', 
                        'data' => 'Aktivace není možná - špatné údaje.'
                    ));
                } catch (PDOException $e) {
                    $this->addMessage(array(
                        'type' => 'error', 
                        'data' => 'Aktivace není možná - chyba DB.'
                    ));
                }
            } else if ($_POST) { // if reg form is confirmed
                try { // try to register
                    $code = sha1(time().$_POST['login']);
                    $u = sha1($code.$_POST['login']);
                    $userManager = new UserManager();
                    $userManager->register($_POST['login'], $_POST['password'], $_POST['password2'], $_POST['name'],$_POST['email'],$_POST['rok'],$code);
                    $email = new EmailSender();
                    $email->sendWithAntispam(
                        $_POST['rok'],
                        $_POST['email'],
                        "Registrace na serveru scholé21.cz",
                        "Dobrý den,<br/><br/>
                        registrace na serveru <a href='https://schole21.weareafk.cz' style='text-decoration:none;color:black;'>Scholé21</a> proběhla <span style='text-weight:bold'>úspěšně</span>.<br/><br/>
                        Přihlašovací jméno: ".$_POST['login']."<br/>
                        E-mail: ".$_POST['email']."<br/><br/>
                        Nyní zbývá jen aktivovat Váš účet kliknutím na odkaz:<br/>
                        <a style='text-weight:bold' href='https://schole21.weareafk.cz/registrace?code=$code&u=$u'>https://schole21.weareafk.cz/registrace?code=$code&u=$u</a><br/><br/>
                        <code>Děkujeme za používání našich služeb, více na <a href='https://schole21.weareafk.cz' style='text-decoration:none;color:black;'>Scholé21.cz</a></code>.
                        ",
                        "noreply@weareafk.cz"
                    );
                    $this->addMessage(array(
                        'type' => 'success',
                        'data' => 'Prosím potvrďte svou registraci kliknutím v odkazu zaslaném na e-mail '. $_POST['email']));
                } catch (ErrorUser $e) { // print all error messages
                    $e = explode(".", $e->getMessage());
                    array_pop($e);
                    foreach($e as $mess) {
                        $mess.=".";
                        $this->addMessage(array(
                            'type' => 'error',
                            'data' => $mess));
                    }
                }
            }
        }
    }
}