<?php
abstract class Controller {
    protected $data = array();
    protected $view = "";
    protected $head = array(
        'title' => '',
        'keywords' => '',
        'description' => '',
    );
    protected $version = '1-000'; // important thing i need to change every version (reload js and css etc)
    protected $rcolu = array(
        'odhlasit-se' => 'Odhlásit se',
        'profil' => 'Profil',
        'balicky' => 'Balíčky'
    );
    protected $pagedescrip = 'Definice, kterou zatím nemáme.';
    protected $nav = array(
        'informace' => 'Informace',
        'jsem-rodic' => 'Jsem rodič',
        'chci-se-ucit' => 'Chci se učit',
        'kontakt' => "Kontakt"
    );
    protected $footer = '<p>&copy; 2021 <a href="https://weareafk.cz/" target="_blank">We are AFK</a></p>
                <p>Použiváním této stránky souhlasíte s <a href="podminky">Podmínkami pro používání</a></p>';

    abstract function run($param); // mandatory function, $param = url
    
    // print view
    public function printView() {
        if ($this->view) {
            extract($this->specialChars($this->data));
            extract($this->data, EXTR_PREFIX_ALL, "");
            require("views/" . $this->view . ".phtml");
        }
    }
    // redirect to other location
    public function redirect($url) {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }
    // return htmlspecialchars
    private function specialChars($x = null) {
        if (!isset($x))
            return null;
        elseif (is_string($x))
            return htmlspecialchars($x, ENT_QUOTES);
        elseif (is_array($x))
        {
            foreach($x as $k => $v)
            {
                $x[$k] = $this->specialChars($v);
            }
            return $x;
        }
        else return $x;
    }

    // return any text to pretty url
    public function textToUrl($source) {
        $table = Array(
            'ä'=>'a','Ä'=>'A','á'=>'a','Á'=>'A','à'=>'a','À'=>'A','ã'=>'a','Ã'=>'A','â'=>'a','Â'=>'A',
            'č'=>'c','Č'=>'C','ć'=>'c','Ć'=>'C',
            'ď'=>'d','Ď'=>'D','ě'=>'e','Ě'=>'E',
            'é'=>'e','É'=>'E','ë'=>'e','Ë'=>'E','è'=>'e','È'=>'E','ê'=>'e','Ê'=>'E',
            'í'=>'i','Í'=>'I','ï'=>'i','Ï'=>'I','ì'=>'i','Ì'=>'I','î'=>'i','Î'=>'I','ľ'=>'l','Ľ'=>'L','ĺ'=>'l',
            'Ĺ'=>'L',
            'ń'=>'n','Ń'=>'N','ň'=>'n','Ň'=>'N','ñ'=>'n','Ñ'=>'N',
            'ó'=>'o','Ó'=>'O','ö'=>'o','Ö'=>'O','ô'=>'o','Ô'=>'O','ò'=>'o','Ò'=>'O','õ'=>'o','Õ'=>'O','ő'=>'o','Ő'=>'O',
            'ř'=>'r','Ř'=>'R','ŕ'=>'r','Ŕ'=>'R',
            'š'=>'s','Š'=>'S','ś'=>'s','Ś'=>'S',
            'ť'=>'t','Ť'=>'T',
            'ú'=>'u','Ú'=>'U','ů'=>'u','Ů'=>'U','ü'=>'u','Ü'=>'U','ù'=>'u','Ù'=>'U','ũ'=>'u','Ũ'=>'U','û'=>'u','Û'=>'U',
            'ý'=>'y','Ý'=>'Y',
            'ž'=>'z','Ž'=>'Z','ź'=>'z','Ź'=>'Z'
        );
        $source = strtr($source, $table);
        $source = iconv("utf-8", "us-ascii//TRANSLIT", $source);
        $source = preg_replace('/\W+/', '-', strtolower(trim($source)));
        if(substr($source,-1) == '-')
            $source = substr($source,0,-1);
        return $source;
    }
    // add message success/warning/error
    public function addMessage($message) {
        if (isset($_SESSION['messages']))
            $_SESSION['messages'][] = $message;
        else
            $_SESSION['messages'][] = $message;
    }
    // return all messages
    public function returnMessages() {
        if (isset($_SESSION['messages'])) {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
            return $messages;
        } else return array();
    }
    // return nav
    public function returnNav() {
        if(isset($this->data['anav']))
            return $this->data['anav'];
        else return null;
    }
    /*
     * 0 - ban
     * 1 - registered
     * 2 - normal user
     * 3 -
     * 4 -
     * 5 -
     * 6 -
     * 7 - 
     * 8 - support admin - cant modify admin rules
     * 9 - admin, all privilegies
     */
    // true if user have privilegies 
    public function verifyUser($role) {
        $userManager = new UserManager();
        $user = $userManager->returnUser();
        if (!$user || ($user['role']<$role)) 
            return false;
        return true;
    }
    // return this view
    public function returnView() {
        return $this->view;
    }
}