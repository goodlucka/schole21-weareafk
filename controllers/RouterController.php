<?php
class RouterController extends Controller {

    protected $controller;

    public function run($param) {
        
        $parsedURL = $this->parseURL($param[0]); // localhost

        if (empty($parsedURL[0]))  // if(localhost) -> redirect to home page
            $this->redirect('home');
        
        $classControllers = $this->dashToCamelCase(array_shift($parsedURL)) . 'Controller'; // which Controller we using

        if (file_exists('controllers/' . $classControllers . '.php'))   // save controller
            $this->controller = new $classControllers;
        else
            $this->redirect('error'); // if controller doesnt exists, redirect to 404 page
        
        $this->controller->run($parsedURL);

        // insert data
        $this->data['title'] = $this->controller->head['title'];
        $this->data['description'] = $this->controller->head['description'];
        $this->data['keywords'] = $this->controller->head['keywords'];
        $this->data['version'] = $this->version;
        $this->data['pagedescrip'] = $this->controller->pagedescrip;
        $this->data['anav'] = $this->controller->returnNav();
        $this->data['nav'] = $this->controller->nav;
        $this->data['messages'] = $this->returnMessages();
        $this->data['footer'] = $this->controller->footer;
        
        // layout matters on user, if he is admin, logged or host
        if(isset($_SESSION['user'])) {
            if($_SESSION['user']['role']==9) {
                $this->view = 'layout_a';
            } else {
                $this->view = 'layout_u';
            }
            $this->data['rcol'] = $this->controller->rcolu;
        } else {
            $this->view = 'layout';
        }
    }

    // parse url from /some/thing/param1/param2 to array with data
    private function parseURL($url) {
        $parsedURL = parse_url($url);   // parse URL by default PHP function
        $parsedURL["path"] = ltrim($parsedURL["path"], "/"); // remove / from left
        $parsedURL["path"] = trim($parsedURL["path"]); // remove whitespace chars
        $splittedData = explode("/", $parsedURL["path"]); // array
        return $splittedData;
    }
    
    // we are using CamelCase but user prefer dashed-url
    private function dashToCamelCase($text) {   // my-url
        $sentence = str_replace('-', ' ', $text);   // my url
        $sentence = ucwords($sentence); // -> My Url
        $sentence = str_replace(' ', '', $sentence); // MyUrl
        return $sentence;
    }
}