<?php
class InformaceController extends Controller {
    public function run($param) {
        $this->head = array(   
            "title" => "Informace",
            "keywords" => "",
            "description" => "Informace o schole21.cz"
        );

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("information");
        $this->data['anav'] = "informace";
        $this->view = "_base";
    }
}