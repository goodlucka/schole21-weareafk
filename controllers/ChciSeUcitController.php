<?php
class ChciSeUcitController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Chci se učit",
            "keywords" => "",
            "description" => "Informace pro studenty na schole21.cz",
            "url" => "chci-se-ucit"
        );

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("for-students");
        $this->data['anav'] = "chci-se-ucit";
        $this->view = "_base";
    }
}