<?php
class HomeController extends Controller {
    public function run($param) {
        $this->head = array(    
            "title" => "Scholé21",
            "keywords" => "",
            "description" => "Hlavní stránka scholé21.cz"
        );

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("home");
        $this->view = "_base";  
    }
}