<?php
class PlayController extends Controller {
    public function run($param) {
        if (isset($param[0])&&$this->verifyUser(9)) {   // if logged user is admin, play/videourl allowed
            $videoManager = new VideoManager();
            if($videoManager->returnVideo($param[0])) { // if video exists, stream it
                $stream = new VideoStream("videos/".$param[0].".mp4");
                $stream->start();
            } else {    // if video doesnt exists, write message
                $this->addMessage(array(
                    "type" => "warning",
                    "data" => "Žádné video nenalezeno"
                ));
            }
        }
        if(isset($param[0])&&isset($param[1])&&isset($_SESSION["user"])) { // play/packurl/videourl
            $videoManager = new VideoManager();
            if($videoManager->returnBoughtVideo($param[0],$param[1])) { // if video exists and it is bought by logged user, stream it
                $stream = new VideoStream("videos/".$param[1].".mp4");
                $stream->start();
            } else {// if video doesnt exists, write message
                $this->addMessage(array(
                    "type" => "warning",
                    "data" => "Žádné video nenalezeno"
                ));
            }
        } else { // no match / user doesnt have pack bought
            $this->addMessage(array(
                "type" => "warning",
                "data" => "Žádné video nenalezeno"
            ));
        }
    }
}