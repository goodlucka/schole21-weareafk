<?php
class ErrorController extends Controller {
    // show error page
    public function run($param) {
        header("HTTP/1.0 404 Not Found");
        $this->head['title'] = 'Chyba 404';
        $this->view = 'error404';
    }
}