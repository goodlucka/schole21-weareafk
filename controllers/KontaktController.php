<?php
class KontaktController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Kontakt",
            "keywords" => "",
            "description" => "Kontaktujte nás skrz formulář.",
            "url" => "kontakt"
        );

        if ($_POST) { // confirmed form
            try { // try to send email
                $from = "";
                if(isset($_SESSION['user'])) 
                    $from = $_SESSION['user']['email'];
                else $from = $_POST["email"];
                $emailSender = new EmailSender();
                $to="afk@weareafk.cz";
                $subject="Scholé21 - kontaktní formulář";
                $emailSender->sendWithAntispam($_POST['rok'], $to, $subject, nl2br($_POST['zprava']), $from); 
                $this->addMessage(array(
                    'type' => 'success', 
                    'data' => 'Email byl úspěšně odeslán.'));
                $this->redirect('kontakt'); // if success, redirect to this page (reset $_POST)
            } catch (ErrorUser $e) { // fail with send email
                $this->addMessage(array(
                    'type' => 'error',
                    'data' => $e->getMessage()));
            }
        }

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("contact");
        $this->data['anav'] = "kontakt";
        $this->view = "contact";
    }
}