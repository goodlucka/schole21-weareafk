<?php
class JsemRodicController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Jsem rodič",
            "keywords" => "",
            "description" => "Informace pro rodiče na schole21.cz",
            "url" => "jsem-rodic"
        );

        $pageManager = new PageManager();
        $this->data["page"] = $pageManager->returnPage("for-parents");
        $this->data['anav'] = "jsem-rodic";
        $this->view = "_base";
    }
}