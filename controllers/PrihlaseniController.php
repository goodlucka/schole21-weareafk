<?php
class PrihlaseniController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Přihlášení",
            "keywords" => "",
            "description" => "Přihlášení na schole21.cz"
        );

        if(isset($_SESSION["user"])) {
            $this->redirect("error"); // redirect to error page
        } else { // only not logged users can see this page
            $userManager = new UserManager();
            $this->head['title'] = 'Přihlášení';
            $this->view = 'login'; // show login form

            if ($_POST) { // if form is send
                try { // try to login
                    $userManager->login($_POST['login'], $_POST['password']);
                    $this->addMessage(array(
                        'type' => 'success',
                        'data' => 'Byl jste úspěšně přihlášen.'));
                    $this->redirect('home'); // if user is succesfully logged, redirect to homepage
                } catch (ErrorUser $e) { // if login failed
                    $this->addMessage(array(
                        'type' => 'error',
                        'data' => $e->getMessage()));
                    $this->view = 'login'; // show login form again
                }
            }
        }
    }
}