<?php
class AdministraceController extends Controller {
    public function run($param) {
        $this->head = array(
            "title" => "Administrace",
            "keywords" => "",
            "description" => "Administrace scholé21.cz"
        );
        if($this->verifyUser(9)) {   // if user is admin
            $pageManager = new PageManager();
            if(isset($param[0])) {  // administrace/something
                try { 
                    switch($param[0]) { // switch something
                        case "podminky": {
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("privacy-policy",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("privacy-policy");
                            $this->view="privacy-policy_a";
                        } break;
                        case "home": { // update home page
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("home",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("home");
                            $this->view="home_a";
                        } break;
                        case "informace": { // update info page
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("information",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("information");
                            $this->view="information_a";
                        } break;
                        case "jsem-rodic": { // update for parents page
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("for-parents",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("for-parents");
                            $this->view="for-parents_a";
                        } break;
                        case "chci-se-ucit": { // update for students page
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("for-students",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("for-students");
                            $this->view="for-students_a";
                        } break;
                        case "kontakt": { // update contact page
                            if(isset($_POST["content"]))
                                $pageManager->updatePage("contact",$_POST["content"]);
                            $this->data["page"] = $pageManager->returnPage("contact");
                            $this->view="contact_a";
                        } break;
                        case "balicky": { // update packs
                            $packManager = new PackManager();
                            if(isset($param[1])) {  // administrace/balicky/something
                                if($param[1]=="new") { // administrace/balicky/new - create new pack
                                    if(isset($_POST["title"])&&isset($_POST["introduction"])) { // if form posted, try to  create new pack
                                        try {
                                            $url = $this->textToUrl($_POST["title"]);
                                            $packManager->newPack($url,$_POST["title"],$_POST["introduction"]);
                                            $this->data["pack"] = $packManager->returnPack($url);
                                            $this->redirect("administrace/balicky/".$url);
                                        } catch (Exception) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při ukládání balíčku."
                                            ));
                                            $this->view="pack-new";
                                        }
                                    } else $this->view="pack-new"; // if form isnt posted
                                } else { // administrace/balicky/purl
                                    if(isset($param[2])) // administrace/balicky/purl/something
                                        if($param[2] == "delete") { // administrace/balicky/purl/delete
                                            try { // try to delete pack
                                                $packManager->deletePack($param[1]);
                                                $this->addMessage(array(
                                                    "type" => "success",
                                                    "data" => "Balíček úspěšně smazán."
                                                ));
                                                $this->redirect("administrace/balicky");
                                            } catch (PDOException) {
                                                $this->addMessage(array(
                                                    "type" => "error",
                                                    "data" => "Nelze smazat balíček, který si někdo koupil."
                                                ));
                                            }
                                        }
                                    if(isset($_POST["introduction"])) { // try to update introduction
                                        try {
                                            $packManager->updatePack($param[1],$_POST["introduction"]);
                                        } catch (PDOException) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při úpravě balíčku."
                                            ));
                                        }
                                    }    
                                    if(isset($_POST["hid"])) {  // if second form is posted - update videos_packs, articles_packs
                                        try {
                                            $packManager->updatePackData($_POST,$param[1]);
                                        } catch (PDOException $e) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při úpravě balíčku."
                                            ));
                                        }
                                    }
                                    $videoManager = new VideoManager();
                                    $articleManager = new ArticleManager();
                                    $this->data["videos"] =  $videoManager->returnVideosInPack($param[1]);
                                    $this->data["articles"] = $articleManager->returnArticlesInPack($param[1]);
                                    $this->data["pack"] = $packManager->returnPack($param[1]);
                                    $this->view="pack_a";
                                }

                            } else { // administrace/balicky - print all packs
                                try {
                                    $this->data["packs"] = $packManager->returnAllPacks();
                                } catch (PDOException) {
                                    $this->addMessage(array(
                                        "type" => "warning",
                                        "data" => "Žádné balíčky nenalezeny."
                                    ));
                                }
                                $this->view="packs_a";
                            }
                        } break;
                        case "videa": { // update videos
                            $videoManager = new VideoManager();
                            if(isset($param[1])) { // administrace/videa/something
                                if($param[1]=="new") { // administrace/videa/new
                                    if(isset($_POST["title"])&&isset($_POST["content"])) { // try to add new video
                                        try {
                                            $url = $this->textToUrl($_POST["title"]);
                                            $videoManager->newVideo($url,$_POST["title"],$_POST["content"]);
                                            $this->data["video"] = $videoManager->returnVideo($url);
                                            $this->redirect("administrace/videa/".$url);
                                        } catch (PDOException) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při ukládání videa, název videa již existuje."
                                            ));
                                            $this->view="video-new";
                                        } catch (Exception $e) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => $e
                                            ));
                                            $this->view="video-new";
                                        }
                                    } else $this->view="video-new"; // print form for adding new video
                                } else {  // administrace/videa/videourl
                                    if(isset($param[2]))  // administrace/videa/videourl/delete
                                        if($param[2] == "delete") { // try to delete video
                                            try {
                                                $videoManager->deleteVideo($param[1]);
                                                $this->addMessage(array(
                                                    "type" => "success",
                                                    "data" => "Video úspěšně smazáno."
                                                ));
                                                $this->redirect("administrace/videa");
                                            } catch (PDOException) {
                                                $this->addMessage(array(
                                                    "type" => "error",
                                                    "data" => "Chyba při mazání videa."
                                                ));
                                            }
                                        }
                                    if(isset($_POST["content"])) { // update video content
                                        try {
                                            $videoManager->updateVideo($param[1],$_POST["content"]);
                                        } catch (PDOException) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při úpravě videa."
                                            ));
                                        }
                                    }
                                    $this->data["video"] = $videoManager->returnVideo($param[1]);
                                    $this->view="video_a";
                                }
                            } else { // administrace/videa - print all videos
                                try {
                                    $this->data["videos"] = $videoManager->returnAllVideos();
                                } catch (PDOException) {
                                    $this->addMessage(array(
                                        "type" => "warning",
                                        "data" => "Žádné videa nenalezeny."
                                    ));
                                }
                                $this->view="videos_a";
                            }

                        } break;
                        case "clanky": { // update articles
                            $articleManager = new ArticleManager();
                            if(isset($param[1])) { // administrace/clanky/something
                                if($param[1]=="new") { // administrace/clanky/new
                                    if(isset($_POST["title"])&&isset($_POST["content"])) { // try to add new article
                                        try {
                                            $url = $this->textToUrl($_POST["title"]);
                                            $articleManager->newArticle($url,$_POST["title"],$_POST["content"]);
                                            $this->data["article"] = $articleManager->returnArticle($url);
                                            $this->redirect("administrace/clanky/".$url);
                                        } catch (Exception) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při ukládání článku."
                                            ));
                                            $this->view="article-new";
                                        }
                                    } else $this->view="article-new"; // show form for adding new article
                                } else { // administrace/clanky/articleurl
                                    if(isset($param[2])) // administrace/clanky/articleurl/delete
                                        if($param[2] == "delete") { // try to delete article
                                            try {
                                                $articleManager->deleteArticle($param[1]);
                                                $this->addMessage(array(
                                                    "type" => "success",
                                                    "data" => "Článek úspěšně smazán."
                                                ));
                                                $this->redirect("administrace/clanky");
                                            } catch (PDOException) {
                                                $this->addMessage(array(
                                                    "type" => "error",
                                                    "data" => "Chyba při mazání článku. Nejdříve jej odeberte ze všech balíčků."
                                                ));
                                            }
                                        }
                                    if(isset($_POST["content"])) { // update article content
                                        try {
                                            $articleManager->updateArticle($param[1],$_POST["content"]);
                                        } catch (PDOException) {
                                            $this->addMessage(array(
                                                "type" => "error",
                                                "data" => "Chyba při úpravě článku."
                                            ));
                                        }
                                    }
                                    $this->data["article"] = $articleManager->returnArticle($param[1]);
                                    $this->view="article_a";
                                }
                            } else { // administrace/clanky print all articles
                                try {
                                    $this->data["articles"] = $articleManager->returnAllArticles();
                                } catch (PDOException) {
                                    $this->addMessage(array(
                                        "type" => "warning",
                                        "data" => "Žádné články nenalezeny."
                                    ));
                                }
                                $this->view="articles_a";
                            }
                        } break;
                    }
                } catch (PDOException) {
                    $this->addMessage(array(
                        'type' => 'error',
                        'data' => 'Chyba při ukládání do databáze.'
                    ));
                }
            } else { // administrace/ - show base page
                $this->data["pages"] = $pageManager->returnAllPages();
                $this->view="administration";
            }
       } else { // not enought privilegies
            $this->addMessage(array(
                'type' => 'warning',
                'data' => 'Nedostatečná oprávnění.'
            ));
            $this->redirect("home");
       }
    }
}