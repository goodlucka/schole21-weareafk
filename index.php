<?php
session_start(); // start session

mb_internal_encoding("UTF-8"); // setting encoding in PHP functions

/**
 * autoload classes which are currently needed
 * @param string $class autoload this class
 */ 
function myAutoloadClass($class) {
    if (preg_match('/Controller$/', $class))
        require("controllers/" . $class . ".php");
    else
        require("models/" . $class . ".php");
}
spl_autoload_register("myAutoloadClass");

DB::connect("localhost", "root", "", "schole21"); // DB settings

// pretty url, print pages
$router = new RouterController();
$router->run(array($_SERVER['REQUEST_URI']));
$router->printView();
